FROM python:3.9.1-alpine AS base
WORKDIR /app

# Prepare dependencies
FROM base AS builder
COPY requirements.txt ./
RUN mkdir /app/libs
RUN apk add --no-cache libgcc git build-base && pip install -r requirements.txt -t /app/libs && \
    rm requirements.txt

# Copy dependency files
FROM base AS production
WORKDIR /app
COPY --from=builder /app /app
ENV PYTHONPATH "${PYTHONPATH}:/app/libs"
ENV PATH "${PATH}:/app/libs/bin"
