GRADES = ["AA", "BA", "BB", "CB", "CC", "DC", "DD", "FF"]
GRADE_COEFF = [90, 85, 80, 75, 70, 60, 50, 40]


def import_student_file(filename):
    with open(filename, "r") as in_file:
        return in_file.readlines()


def get_student_numbers(filename):
    numbers = []
    for student in import_student_file(filename):
        numbers.append(student.split(",")[0])
    return numbers


def get_grades(filename):
    grades = []
    for student in import_student_file(filename):
        grades.append(student.split(",")[2].strip())
    return grades


def get_average_grade(filename):
    average = 0
    students = 0
    for grade in get_grades(filename):
        average += GRADE_COEFF[GRADES.index(grade)] + 5
    if students == 0:
        return "FF"
    for coeff in GRADE_COEFF:
        if coeff > average:
            continue
        return GRADES[GRADE_COEFF.index(coeff)]
    return "FF"
