def pytest_json_modifyreport(json_report):
    del json_report["root"]
    del json_report["environment"]
    del json_report["collectors"]
