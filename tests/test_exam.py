from user_code import get_average_grade, get_grades, get_student_numbers


def test_number_of_students_small_file():
    assert len(get_student_numbers("students.txt")) == 5


def test_number_of_students_large_file():
    assert len(get_student_numbers("students_large.txt")) == 24
