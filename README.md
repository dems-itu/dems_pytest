dems_pytest 🔬🐍
==============

  [![pipeline status](https://gitlab.com/dems-itu/dems_pytest/badges/master/pipeline.svg)](https://gitlab.com/dems-itu/dems_pytest/-/pipelines)

Python tester module with pytest for the Digital Exam Management System.

# Usage

```bash
# build the image
$ docker build -t dems/pytest .
# make sure the image "dems/pytest" is in the images list
$ docker images
# run the tests. Use "pwd" command to find the full path
$ docker run -v /full_path_to/tests:/app/files:ro dems/pytest /bin/sh -c 'cd /app/files/; pytest --disable-pytest-warnings --json-report --json-report-omit keywords streams --json-report-file=/tmp/result.json . >> /dev/null; cat /tmp/result.json; rm /tmp/result.json'
```

# Additional libraries

These additional libraries can be used for tests:
* [requests-mock](https://requests-mock.readthedocs.io/en/latest/pytest.html)
* [requests](https://requests.readthedocs.io/en/master/)

## COPYRIGHT

MIT License. Copyright (c) 2021 Emin Mastizada.

Check the LICENSE file provided with the project for the details.
